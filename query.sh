#!/bin/sh


if [ -z ${metadata_key} ] 
then
    #echo "${viewer_perm}" > sa.json
    #export GOOGLE_APPLICATION_CREDENTIALS="sa.json"

    echo -e "****Listing Image used****";
    curl -s "http://metadata.google.internal/computeMetadata/v1/instance/image" -H "Metadata-Flavor: Google";

    echo -e "\n****Listing disk used****";
    curl -s "http://metadata.google.internal/computeMetadata/v1/instance/disks/" -H "Metadata-Flavor: Google";

    echo -e "****Listing disk 0****";
    curl -s "http://metadata.google.internal/computeMetadata/v1/instance/disks/0/" -H "Metadata-Flavor: Google";

    echo -e "****disk 0 type****";
    curl -s "http://metadata.google.internal/computeMetadata/v1/instance/disks/0/type" -H "Metadata-Flavor: Google";

    echo -e "\n****list disks****";
    curl -s "http://metadata.google.internal/computeMetadata/v1/instance/disks/?recursive=true" -H "Metadata-Flavor: Google";

    #By default, recursive contents are returned in JSON format. If you want to return these contents in text format, append the alt=text query parameter:

    curl -s "http://metadata.google.internal/computeMetadata/v1/instance/tags" -H "Metadata-Flavor: Google";

else

    ## User Specific query
    echo -e "******USER QUERY FOR ${metadata_key}******"
    curl -s "http://metadata.google.internal/computeMetadata/v1/instance/${metadata_key}" -H "Metadata-Flavor: Google";

fi

